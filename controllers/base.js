
exports.create = async (req, res, next) => {
		const data = req.body
		const User = new req.Model(data);
		const result = await User.save();
		if(result){
				return res.sendResult(User,201,'注册成功')
			}
		return res.sendResult(err,400,"注册失败");
}
exports.list = async (req, res, next) => {
		const data = {
			query: req.body.query ? req.body.query : {},
			 page : req.body.page ? req.body.page : 1,
			  per : req.body.pageSize ? req.body.pageSize : 10			
		}
		const { query,per,page } = data
    const result = await req.Model.find(query).limit(per).skip((page - 1) * per).sort({
        _id: -1,
      });
    const totalCount = await req.Model.countDocuments(query);
    res.sendResult({
      totalCount,
      pages: Math.ceil(totalCount / per),
      result,
    },200,"查询成功");
}
exports.destroy = async (req, res, next) => {
		const result = await req.Model.findByIdAndDelete(req.params.id);
		if(result){
				return res.sendResult(result,200,'删除成功')
			}
		return res.sendResult(err,400,"删除失败");
}
exports.update = async (req, res, next) => {
		// console.log(req.body)
		const result = await req.Model.findByIdAndUpdate(req.params.id,req.body)
		if(result){
				return res.sendResult(result,200,'更新成功')
			}
		return res.sendResult(err,400,"更新失败");
}
exports.show = async (req, res, next) => {
	const result = await req.Model.findById(req.params.id)
  if(result){
		return res.sendResult(result,200,'查询成功')
	}
	return res.sendResult(err,400,"查询失败");
}