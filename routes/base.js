var express = require("express");
var router = express.Router();
const BaseController = require('../controllers/base.js')

const findModel =function(req,res,next){
		const modelName= require('inflection').classify(req.params.rest) 
    req.Model = require(`../models/${modelName}`)
  next();
}


router.post("/:rest/create",findModel ,BaseController.create);
router.delete("/:rest/:id",findModel, BaseController.destroy);
router.put("/:rest/:id",findModel, BaseController.update);
router.get("/:rest/list",findModel, BaseController.list);
router.get("/:rest/:id",findModel, BaseController.show);

module.exports = router;