const mongoose = require('mongoose');
// mongoose.connect('mongodb://localhost/test');
exports.Mongoose = {
  url: "mongodb://localhost:27017/many",
  connect() {
    mongoose.connect(
      this.url,
      { useNewUrlParser: true, useFindAndModify: false , useUnifiedTopology: true},
      (err) => {
        if (err) {
          console.log("数据库连接失败");
          return;
        }
        console.log("数据库连接成功");
      }
    );
  },
};


