const express = require('express')
const bodyParser = require('body-parser')
const db = require('./config/db.js')

const app = express()

const server = app.listen(8082)
const io = require('socket.io').listen(server)

io.on('connection', (socket) => {
	console.log('a user connected');

	socket.on('message',data => {
		// console.log(data)
		socket.broadcast.emit('glmsg',data);
	});
});

app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
const port = 3000

const resextra = require('./middleware/resextra')
app.use(resextra)

app.get('/', (req, res) => {
	res.sendResult(111,200,"hello");
})

app.use('/api',require('./routes/base.js'))
app.use('/api',require('./routes/users.js'))

db.Mongoose.connect();

app.use(function (req, res, next) {
  let err = new Error("Not Found");
  err.status = 404;
  next(err);
});

//错误处理
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.send(err.message);
});

app.listen(port,() => console.log(`Example app listening on port ${port}!`))